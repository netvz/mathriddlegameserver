package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DatabaseConnection {
    static Connection connection;
    private List<String> operations = new ArrayList<>();
    private Random rand = new Random();

    public static Connection getConnection() {
        return connection;
    }

    public void connect(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\godzi\\OneDrive\\Pulpit\\mathRiddles");
            System.out.println("Connected");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void disconnect(){
        try {
            connection.close();
            System.out.println("Disconnected");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void populate() throws SQLException {
        Statement statement = DatabaseConnection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM calculations");
        while(resultSet.next()) {
            String operation = resultSet.getString("calculation");
            operations.add(operation);
        }
    }

    public String get() {
        return operations.get(rand.nextInt(operations.size()));
    }

}
