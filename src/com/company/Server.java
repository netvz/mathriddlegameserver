package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Server extends Thread {
    ServerSocket server;
    List<ClientThread> clients;
    DatabaseConnection riddles;

    public Server(int port, DatabaseConnection connectino){
        try {
            server = new ServerSocket(port);
            riddles = connectino;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run(){
        Socket clientSocket;
        clients = new ArrayList<>();
        while(true){
            try {
                clientSocket = server.accept();
                ClientThread client = new ClientThread(clientSocket, this);
                clients.add(client);
                client.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeClient(ClientThread client) {
        clients.remove(client);
        System.out.println("removed");
    }

    public void broadcast(String message){
        for(var client : clients)
            client.send(message);

    }

    public void startSending() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                broadcast(riddles.get());
            }
        }, 0, 5000);
    }

}
