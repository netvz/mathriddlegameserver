package com.company;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        DatabaseConnection riddles = new DatabaseConnection();
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        riddles.connect();
        try {
            riddles.populate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Server server = new Server(4200, riddles);
        server.start();
        server.startSending();
        riddles.disconnect();
    }
}
